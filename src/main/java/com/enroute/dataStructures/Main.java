package com.enroute.dataStructures;

public class Main {
	
	public static void main(String[] args) {
		
		DoublyLinkedList DoublyLinkedList = new DoublyLinkedList();

		DoublyLinkedList.add(1);
		DoublyLinkedList.add(2);
		DoublyLinkedList.add(3);
		DoublyLinkedList.add(4);

		System.out.println("Linked List before removing item (3)");
		DoublyLinkedList.printDLL();

		System.out.println("Linked List after removing item (3)");
		DoublyLinkedList.remove(3);
		DoublyLinkedList.printDLL();

	}
}
