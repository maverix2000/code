package com.enroute.dataStructures;

/* Raul barranco C. */
import java.util.*;

public class Node
{

	public Node prev;
	public Node next;
	public int val;

	public Node(int val)
	{
		this.val = val;
		prev = null;
		next = null;
	}

}