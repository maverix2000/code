package com.enroute.dataStructures2;

/* Raul Barranco C. */

public class Pair<T, U> extends Mapa<T>
{
	int key;
	int value;

	Pair(int key, int value)
	{
		this.key = key;
		this.value = value;
	}

	int getKey()
	{
		return this.key;
	}

	int getValue()
	{
		return this.value;
	}
	
}