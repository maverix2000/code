package com.enroute.dataStructures2;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.util.Pair;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Collections;

public class Mapa<T>
{ 

	Map <Integer, List<Pair<Integer, Integer>>> mapa;

	Mapa()
	{
		mapa = new HashMap<Integer, List<Pair<Integer, Integer>>>();
	}
	
	void set(int key, int value, int time)
	{
		mapa.putIfAbsent(key, new LinkedList<Pair<Integer, Integer>>());
		LinkedList<Pair<Integer,Integer>> ll = (LinkedList<Pair<Integer,Integer>>)mapa.get(key);
		if(ll.contains(new Pair<Integer,Integer>(value, time)))
			return;
		ll.add(new Pair<Integer,Integer>(value, time));
	}

	void print()
	{
		for (Integer name: mapa.keySet()){
            String key = name.toString();
            String value = mapa.get(name).toString();  
            System.out.println(key + " " + value);  
		} 
		
	}

	int get(int key, int time)
	{
		if(!mapa.containsKey(key))
		{
			System.out.println("map does not contain key");
			return -1;
		}

		LinkedList<Pair<Integer,Integer>> ll = (LinkedList<Pair<Integer,Integer>>)mapa.get(key);

		int temp = Integer.MIN_VALUE;
		int r = -1;	
		for(Pair p: ll)
		{
			if((Integer)p.getValue()<=time && (Integer)p.getValue()>temp)
			{
				temp = (Integer)p.getValue();
				r = (Integer)p.getKey();
			}
		}

		return r;
	}

}