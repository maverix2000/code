package com.enroute.dataStructures2;

import java.util.*;
import java.lang.*;

public class SetSum
{
	static boolean setSum(int [] arr, int sum)
	{
		HashSet<Integer> h = new HashSet<Integer>();

		for(int i: arr)
		{
			int temp = sum-i;

			if(h.contains(temp))
				return true;

			h.add(i);
		}
		return false;
	}


	public static void main(String[] args) {
		
		int [] arr = {10,20,30,40,50,60,70,80,90,100};

		int sum = 151;

		System.out.println("Sum adds up?: "+setSum(arr, sum));

	}

}