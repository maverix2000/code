import java.util.*;

public class SetBubbleSort
{

	static void setBubbleSort(HashSet<Integer> h)
	{

		List<Integer> arr = new ArrayList<Integer>(h);

		for(int i=0; i<arr.size(); i++)
		{
			for(int j=0; j<arr.size()-i-1; j++)
			{
				if(arr.get(j)>arr.get(j+1))
				{
					int temp = arr.get(j);
					arr.set(j, arr.get(j+1));
					arr.set(j+1, temp);
				}
			}
		}

		System.out.println(arr);

		h = new HashSet<Integer>(arr);

		System.out.println(h);

	}


	public static void main(String[] args) {
		

		Random aleatorio = new Random();

		HashSet<Integer> h = new HashSet<Integer>();

		for(int i=0; i<20; i++)
		{
			int aux = aleatorio.nextInt(1000);
			System.out.println(aux);
			h.add(aux);

		}

		System.out.println(h);

		setBubbleSort(h);

	}
	
}