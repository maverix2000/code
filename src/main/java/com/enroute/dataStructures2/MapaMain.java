package com.enroute.dataStructures2;

public class MapaMain {
	
	public static void main(String[] args) {

		Mapa mapa1 = new Mapa();

		mapa1.set(1,1,0);
		mapa1.set(1,2,2);
		mapa1.set(1,4,5);
		mapa1.set(1,8,10);

		mapa1.print();

		System.out.println(mapa1.get(1,10));
		
	}

}
