package com.enroute.dataStructures2;

import java.util.*;
import java.lang.*;

public class SetSum2
{
	static boolean setSum2(ArrayList<Integer> list, int sum)
	{
		if(list.isEmpty())
			return false;
		int l=0;
		int r=list.size()-1;

		Collections.sort(list);

		while(l<r)
		{
			if(list.get(l)+list.get(r)==sum)
				return true;
			if(list.get(l)+list.get(r)<sum)
				r--;
			else
				l++;
		}

		return false;
	}

	public static void main(String[] args) {

		ArrayList<Integer> list = new ArrayList<Integer>();

		list.add(90);
		list.add(70);
		list.add(50);
		list.add(30);
		list.add(10);
		list.add(20);
		list.add(40);
		list.add(60);
		list.add(80);
		list.add(100);

		int sum = 151;

		System.out.println("Sum adds up?: "+setSum2(list, sum));
		
	}

}