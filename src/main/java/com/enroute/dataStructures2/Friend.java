package com.enroute.dataStructures2;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class Friend {
    private Collection<Friend> friends;
    private String email;

    public Friend(String email) {
        this.email = email;
        this.friends = new ArrayList<Friend>();
    }

    public String getEmail() {
        return email;
    }

    public Collection<Friend> getFriends() {
        return friends;
    }

    public void addFriendship(Friend friend) {
        friends.add(friend);
        friend.getFriends().add(this);
    }
    
    public boolean canBeConnected(Friend friend, HashSet<String> visited) {

        if(visited.contains(this.getEmail()))
            return false;
        if(this.friends.contains(friend))
            return true;
        visited.add(this.getEmail());
        System.out.println(visited.toString());
        
        for(int i=0;i<this.friends.size();i++)
        {
            if(((ArrayList<Friend>) this.friends).get(i).canBeConnected(friend, visited))
                return true;
        }
        
        return false;
    }

    public static void main(String[] args) {
        Friend a = new Friend("A");
        Friend b = new Friend("B");
        Friend c = new Friend("C");
        Friend d = new Friend("D");
        Friend e = new Friend("E");

        a.addFriendship(b);
        b.addFriendship(e);
        e.addFriendship(c);
        c.addFriendship(d);

        HashSet<String> visited = new HashSet<String>();

        System.out.println(a.canBeConnected(c, visited));
    }
}