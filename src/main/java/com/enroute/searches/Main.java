package com.enroute.searches;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {

		BinarySearch linkedlist = new BinarySearch();

		linkedlist.add(1);
		linkedlist.add(2);
		linkedlist.add(3);
		linkedlist.add(4);
		linkedlist.add(5);

		linkedlist.print();

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter number (integer) to find in linked list");
		int n = sc.nextInt();
		System.out.println("Number ["+n+"] found on linked list: "+linkedlist.binarySearch(n));
		
	}

}
