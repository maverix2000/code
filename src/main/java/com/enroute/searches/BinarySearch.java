package com.enroute.searches;

/* Raul Barranco C. */
import com.enroute.dataStructures.Node;
import java.util.*;

class BinarySearch
{

	Node head;

	BinarySearch()
	{
		head = null;
	}

	void add(int val)
	{ 
		Node newnode = new Node(val);
		Node current = head;

		if(current==null)
		{
			head = newnode;
			return;
		}

		while(current.next!=null)
		{
			current = current.next;
		}

		current.next = newnode;
		return;

	}

	void print()
	{
		Node current = head;

		while(current!=null)
		{
			System.out.print(", "+current.val);
			current = current.next;
		}
		System.out.println();
	}

	boolean binarySearch(int val)
	{
		if(head==null)
			return false;

		Node first = head;
		Node last = null;

		Node medium = null;

		int counter = 0;
		while(true)
		{
			counter++;
			if(first == null)
				return false;
			if(first.next==null || first.next==last)
			{
				if(first.val == val)
					return true;
				else
					return false;
			}

			medium = findMedium(first, last);

			if(medium.val == val)
				return true;

			if(medium.val<val)
			{
				first = medium.next;
			}

			if(medium.val>val)
			{
				last = medium.next;
			}
		}

	}

	Node findMedium(Node first, Node last)
	{
		Node slowptr, fastptr;
		slowptr = fastptr = first;

		while(fastptr.next != last && fastptr.next.next != last)
		{
			slowptr = slowptr.next;
			fastptr = fastptr.next.next;
		}

		return slowptr;
	}

}